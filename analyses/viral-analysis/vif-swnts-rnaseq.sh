#!/bin/bash
FILES="/home/ec2-user/swnts_data/*_1.merged.fastq.gz"

for f in $FILES
do
        echo "Processing $f file..."
        file1=$(basename "$f")
        echo "$file1"

        substring=${file1%_1.merged.fastq.gz}

        #42_S42_L001_R1_001.fastq.gz

        #substring=$(echo $file1| cut -d'_' -f 1)
        echo $substring

        file2=$substring"_2.merged.fastq.gz"
        echo $file2

        dirname=$substring
        echo $dirname

        docker run -v $HOME:/data --rm trinityctat/ctat_vif@sha256:5d2f3f195bfac9bdd9e035bf9b8ba67d8616cceb589185b31994cbb2d7e1eb69 \
                /usr/local/bin/ctat-vif \
                --sample $dirname \
                --left /data/swnts_data/$file1 \
                --right /data/swnts_data/$file2 \
                --genome_lib_dir /data/VIF/GRCh37_gencode_v19_CTAT_lib_Mar012021.plug-n-play/ctat_genome_lib_build_dir \
                --viral_fasta /data/VIF/virus_db.nr.fasta \
                --outputdir /data/VIF/results_swnts/$dirname/ \
                --cpu 16\

done
