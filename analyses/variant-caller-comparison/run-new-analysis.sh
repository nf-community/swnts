
# Intend to merge many of these scripts as the analysis expands to include somatic
# events and WGS. Current analysis only includes germline WES

### BLOOD SAMPLES
## Evaluate whether HaplotypeCaller, DeepVariant, or a combination of the two
## is most consistent with expected results
#Use vcftools to compare vcfs from different tools
#bash 01-preprocess-data.sh

#Parse vcftools output to collect 1) call count data and 2) transition/transversion count data from germline calls
#bash 02-merge-processed-data.sh

#Create plots of germline call count data and transition - transversion ratios
#Rscript 03-germline-comparison-plots.R

#Create consensus vcfs
#bash 04-wes-consensus-vcfs.sh

#Extract VAFs
#	echo -e "Sample\tVAF\tDepth\tVariantType" > ../../scratch/blood.ConsensusVafs.tsv
#while read -r sample; do
#	python3 05-parse-vcfs.py -v ../../scratch/${sample}.DV.HC.consensus.vcf >> ../../scratch/blood.ConsensusVafs.tsv
#done < ../../scratch/samples.txt

#Perform additional QC and apply filters
Rscript 06-wes-germline-filter-plots.R


