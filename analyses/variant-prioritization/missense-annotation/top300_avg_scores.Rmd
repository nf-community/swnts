---
title: "top300_avg_scores"
author: "huixin"
date: "2023-09-09"
output: html_document
---
```{r setup, include=FALSE}
library(readr)
library(dplyr)
library(ggplot2)
library(gridExtra)
library(kableExtra)
library(reshape2)
library(LSD)
library(R.utils)
```

# 
```{r cars}
patient10_avg_score <- read_tsv('../dataset/swn_patient_10_blood.vcf.variant.tsv')
patient11_avg_score <- read_tsv('../dataset/swn_patient_11_blood.vcf.variant.tsv')
patient2_avg_score <- read_tsv('../dataset/swn_patient_2_blood.vcf.variant.tsv')
patient1_avg_score <- read_tsv('../dataset/swn_patient_1_blood.vcf.variant.tsv')

# How many NAs for each columns?
patient10_avg_score%>%
  summarise_all(~ sum(is.na(.)))

# How many distinct chromosome and positions combinations?
# 44,349 rows of data 
patient10_avg_score %>%
  summarise(unique_chromosomes = n_distinct(Chromosome),
            unique_positions = n_distinct(Position))

# How many duplicated chromosome and position combinations?
patient10_avg_score %>%
  group_by(Chromosome, Position) %>%
  filter(n() > 1)
```

# 
```{r}
# What are the top 300 Chrom and Pos based on the average scores?
get_top_300_scores <- function(input_df) {
  top_300 <- input_df %>% arrange(desc(average_scaled_score)) %>% slice(1:300)
  return(top_300)
}

# What does dist of the scores look like?
create_histogram_plot <- function(input_df, p) {
# Calculate mean and median
mean_value <- mean(input_df$average_scaled_score)
median_value <- median(input_df$average_scaled_score)

  plot <- input_df %>%
    group_by(Chromosome, Position) %>%
    ggplot(aes(x = average_scaled_score)) +
    geom_histogram(binwidth = 0.01, fill = "blue", alpha = 0.5) +
    labs(title = paste("Score distribution of P", p), x = "Score", y = "Frequency") +
    theme_minimal()+
  geom_vline(xintercept = mean_value, color = "red", linetype = "dashed", size = 1) +
  geom_vline(xintercept = median_value, color = "green", linetype = "dashed", size = 1) +
  geom_text(
    aes(x = mean_value, label = paste("μ =", round(mean_value, 3)), y = 10),
    color = "red",
    vjust = -1,
    hjust = -0.5
  ) +
  geom_text(
    aes(x = median_value, label = paste("Med =", round(median_value, 3)), y = 10),
    color = "green",
    vjust = -2.5,
    hjust = -0.5
  ) 
  return(plot)
}

# What are the top 300 Chrom and Pos based on the average scores?
p10_top300<-get_top_300_scores(patient10_avg_score)
p11_top300<-get_top_300_scores(patient11_avg_score)
p1_top300<-get_top_300_scores(patient1_avg_score)
p2_top300<-get_top_300_scores(patient2_avg_score)

# Histogram for p11
p11_plot <- create_histogram_plot(p11_top300, 11)
p10_plot <- create_histogram_plot(p10_top300, 10)
p1_plot <- create_histogram_plot(p1_top300, 1)
p2_plot <- create_histogram_plot(p2_top300, 2)
p<- grid.arrange(p1_plot, p2_plot, p10_plot,p11_plot,  ncol = 2)

ggsave("../figures/dist_avg_score_four_patients.jpg", plot = p, width = 6, height = 4, dpi = 800)

```
```{r}
df1<- p11_top300
df2 <- p10_top300

# Combine the two data frames
combined_df <- bind_rows(p11_top300, p10_top300, p1_top300, p2_top300)

```

```{r}
# Count the instances of each unique combination of Chromosome and Position
count_result <- combined_df %>%
  group_by(Chromosome, Position, average_scaled_score) %>%
  summarise(Count = n())

# the average score for the same chr and pos is different, why
# for example, when we examine chr1, 54716627 
count_result %>% arrange(Chromosome, Count, average_scaled_score) %>% filter(Chromosome == 'chr1', Position == 54716627)
```

```{r}
# Count the instances of each unique combination of Chromosome and Position
count_result <- combined_df %>%
  group_by(Chromosome, Position) %>%
  summarise(Count = n())

# How many times does this chr and pos appear across all four patients?
count_result %>% arrange(Chromosome, Position, Count) %>% filter(Count == 4)
```
 