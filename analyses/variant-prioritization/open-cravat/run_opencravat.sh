#!/bin/bash
#loop through all the patients vcf files and annotate
while read line
do
  echo $line
  oc run $line -l hg38 -a vest revel provean mutpred1 mutationtaster dbscsnv cadd cadd_exome aloft  -d $HOME/opencravat_output/ -t vcf
  
done < vcf_files.txt

#Below are the annotations I used
# vest
# revel.
# provean.
# mutpred1.
# mutationtaster.
# dbscsnv.
# cadd.
# aloft.
