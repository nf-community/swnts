
library(ggplot2)
library(tidyr)
library(dplyr)
library(scales)
library(sagethemes)
library(extrafont)
library(showtext)
showtext_auto()

#import_lato() # For use with sagethemes ?


#######
# Call counts
# https://gatk.broadinstitute.org/hc/en-us/articles/360035531572-Evaluating-the-quality-of-a-germline-short-variant-callset
# GATK reports that the expected number of germline variants
# in WES data is ~41k
#######

#Read in file create by step 02-merge-processed-data
df <- read.csv("../../scratch/blood.toolComparisonSummary.tsv",
              header=T,
              sep="\t")

long <- df %>%
  gather(Source,Count,-c(PatientNo,DataSource))

# Plot the total counts of common/unique calls
# Not that we consistently see ~50,000 calls common to both tools,
# which account for most HaplotypeCaller SNVs, but relatively few
# of the DeepVariant calls.
# This is similar to the expectations from GATK

gatk_expected = 41000

p1 <- ggplot() +
  geom_bar(data=long,
           aes(x=reorder(factor(PatientNo),-Count),
               y=Count,
               fill=factor(Source,levels=c("Unique.HC","Unique.DV","CommonCalls"))),
           stat="identity") +
  geom_hline(yintercept=gatk_expected) + 
  scale_y_continuous(expand=c(0,0),labels=scales::comma,
                     breaks=c(250000,500000,750000,1000000,1250000,1500000,1750000)) +
  scale_fill_sage_d(labels = c("Unique to HaplotypeCaller",
                               "Unique to DeepVariant",
                               "Common to Both")) +
  theme_sage() +
  labs(fill="",
       x="Patient ID",
       y="# of Germline Calls",
       title="Comparison of Counts from Germline Variant Callers",
       subtitle="Blood Samples",
       caption="Horizontal line indicates expected # of germline variants in WES per GATK guidelines (~41k variants)")

ggsave("plots/BloodSamples.WES.CallCounts.pdf",
       device="pdf",
       plot = p1,
       height = 7,
       width = 12,
       units = "in")




#######
# Transition to Transversion Ratio
# https://gatk.broadinstitute.org/hc/en-us/articles/360035531572-Evaluating-the-quality-of-a-germline-short-variant-callset
# GATK reports that the expected TiTv Ratio in humans
# ranges from ~2.0-2.1 (WGS) to ~3.0-3.3 (WES),
# although the value for WES may be slightly lower if
# less of the flanking CpG islands near promoter
# regions are included
#######

dat <- read.csv("../../scratch/blood.toolTiTvRatio.tsv",
                sep="\t",
                header=T)

# Total events for a tool are the sum of unique events + common events
dat$Transitions.AllDV <- dat$Transitions.DV + dat$Transitions.Common
dat$Transitions.AllHC <- dat$Transitions.HC + dat$Transitions.Common
dat$Transversions.AllDV <- dat$Transversions.DV + dat$Transversions.Common
dat$Transversions.AllHC <- dat$Transversions.HC + dat$Transversions.Common

# Calculate TiTv ratio for the different groupings/tools
dat$UniqueDV.Ratio <- dat$Transitions.DV / dat$Transversions.DV
dat$AllDV.Ratio <- dat$Transitions.AllDV / dat$Transversions.AllDV
dat$UniqueHC.Ratio <- dat$Transitions.HC / dat$Transversions.HC
dat$AllHC.Ratio <- dat$Transitions.AllHC / dat$Transversions.AllHC
dat$Common.Ratio <- dat$Transitions.Common / dat$Transversions.Common

gatk_wes_upper_expectation = 3.3
gatk_wes_lower_expectation = 3.0


p2 <- ggplot() +
  geom_jitter(data=dat,
             aes(x="Unique\nDeepVariant\nCalls",
                 y=UniqueDV.Ratio),
             width=0.05) +
  geom_jitter(data=dat,
              aes(x="Unique\nHaplotypeCaller\nCalls",
                  y=UniqueHC.Ratio),
              width=0.05) +
  geom_jitter(data=dat,
              aes(x="Union\nof all\nCalls",
                  y=Common.Ratio),
              width=0.05) +
  geom_jitter(data=dat,
              aes(x="All\nDeepVariant\nCalls",
                  y=AllDV.Ratio),
              width=0.05) +
  geom_jitter(data=dat,
              aes(x="All\nHaplotypeCaller\nCalls",
                  y=AllHC.Ratio),
              width=0.05)+
  geom_boxplot(data=dat,
              aes(x="Unique\nDeepVariant\nCalls",
                  y=UniqueDV.Ratio),
              alpha=0.5,
              outlier.shape=NA) +
  geom_boxplot(data=dat,
              aes(x="Unique\nHaplotypeCaller\nCalls",
                  y=UniqueHC.Ratio),
              alpha=0.5) +
  geom_boxplot(data=dat,
              aes(x="Union\nof all\nCalls",
                  y=Common.Ratio),
              alpha=0.5) +
  geom_boxplot(data=dat,
              aes(x="All\nDeepVariant\nCalls",
                  y=AllDV.Ratio),
              alpha=0.5,
              outlier.shape=NA) +
  geom_boxplot(data=dat,
              aes(x="All\nHaplotypeCaller\nCalls",
                  y=AllHC.Ratio),
              alpha=0.5) +
  geom_hline(yintercept=gatk_wes_lower_expectation) +
  geom_hline(yintercept=gatk_wes_upper_expectation) +
  scale_x_discrete(limits=c("Unique\nDeepVariant\nCalls",
                            "All\nDeepVariant\nCalls",
                            "Unique\nHaplotypeCaller\nCalls",
                            "All\nHaplotypeCaller\nCalls",
                            "Union\nof all\nCalls")) +
  scale_y_continuous(limits=c(0,3.5)) +
  theme_sage() +
  labs(x="Source of Call",
       y="TiTv Ratio",
       title="Transition to Transversion Ratio of Different Germline Callers",
       subtitle="Blood Samples",
       caption="Top and bottom horizontal lines indicate expected range of values per GATK guidelines.
       Expected values may vary based on size of flanking regions used (2.0 expected for WGS, 0.5 expected for pure noise).")


ggsave("plots/BloodSamples.WES.TiTvRatio.pdf",
       device="pdf",
       plot = p2,
       height = 7,
       width = 12,
       units = "in")












