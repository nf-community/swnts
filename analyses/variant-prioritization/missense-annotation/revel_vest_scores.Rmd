---
title: "R Notebook"
output:
  html_document:
    df_print: paged
---
 
```{r, echo=FALSE, message = FALSE}
library(readr)
library(dplyr)
library(ggplot2)
library(kableExtra)
library(reshape2)
library(LSD)
library("sagethemes")
```

```{r, message=FALSE }


revel_df  <- read_tsv('../dataset/all_patients_revel_score.tsv', col_names  = FALSE)
colnames(revel_df) <- c("chr",  "pos",  "revel", "pid")
write.table(revel_df, file = '../dataset/all_patients_revel_score_wcolnames.tsv', sep = "\t", row.names = FALSE)
 


revel_df <- revel_df%>% group_by(revel, pos) %>% dplyr::select(-pid) %>% unique()
revel_df$revel <-as.numeric(revel_df$revel)
revel_df <- revel_df %>% melt(id = c('pos', 'chr')) %>% mutate_all(~replace(., . == ".", NA)) %>% na.omit()


thresholds <- c(0.1,   0.1, 0.25, 0.33, 0.5, 0.66, 0.75)

# Group data by variable and count values below each threshold
df_counts <- revel_df   %>%
  summarise( counts_less_0.1 = sum(value < thresholds[1]),
             counts_0.1 = sum(value >= thresholds[2]),
    counts_0.25 = sum(value  >= thresholds[3]),
            counts_0.33 = sum(value >= thresholds[4]),
            counts_0.5 = sum(value  >= thresholds[5]),
            counts_0.66 = sum(value  >= thresholds[6]),
    counts_0.75 = sum(value >= thresholds[7]),
   )
df_counts
# View resulting counts
# Melt the data frame to long format
df_melted <- melt(df_counts, variable.name = "threshold", value.name = "count")
prop.table(df_melted$count)
# Create the bar plot
jpeg("../figures/all_patients_revel_threshold_count.jpeg")

ggplot(df_melted, aes(x = threshold, y = count, fill= threshold)) +
  scale_color_sage_d() +
  geom_bar(stat = "identity", position = "dodge")  +
  scale_fill_sage_d( labels = c('<0.1',">=0.1" ,">=0.25" ,">=0.33" ,">=0.5" ,">=0.66",  ">=0.75"  )) +
  theme_sage()  + labs(x = "Thresholds", y = "Count", title = "Counts of REVEL Scores Above Custom Thresholds across all patients ") +
  theme_classic()+
  theme(legend.position = 'none') +
  scale_x_discrete(labels = c('<0.1', '>=0.1', '>=0.25', '>=0.33', '>=0.5', '>=0.66', '>=0.75'))
dev.off()

```

```{r}

revel <- read_tsv('../dataset/oc_standalone_combined_REVEL.tsv', col_names  = FALSE)
colnames(revel) <- c("chr",  "pos",  "revel_oc", "revel_standalone")
revel <- revel %>% mutate_all(~replace(., . == ".", NA)) %>% na.omit()

revel$revel_oc <- revel %>% 
  dplyr::select(revel_oc) %>% 
  pull() %>% 
  as.numeric()
revel <- revel %>% dplyr::select(-c("revel_standalone"))
mean_data <- aggregate(revel$revel_oc,  by = list(revel$chr), FUN = mean)
median_data <- aggregate(revel$revel_oc, by = list(revel$chr), FUN = median)

 

vest <- read_tsv('../dataset/openCRAVAT_VEST_output.tsv', col_names  = FALSE)
 colnames(vest) <- c("chr",  "pos",  "vest_oc", "vest_pval")
vest <- vest %>% mutate_all(~replace(., . == ".", NA)) %>% na.omit()
vest$vest_oc <- vest %>% 
  dplyr::select(vest_oc) %>% 
  pull() %>% 
  as.numeric() 

vest <- vest %>% dplyr::select(-c("vest_pval"))
df <- merge(revel, vest )
df <- df %>% melt(id = c('pos', 'chr'))

jpeg("../figures/oc_REVEL_VEST_swnt_patient_1_blood_chr21.jpeg")
ggplot(df, aes(x=value, fill=variable)) +
  geom_histogram(alpha=0.5, position="identity", bins=25)  +
  labs(x="Score", y="Count", title="Histogram of Scores by Tool (Patient 1 Chr21)")    +
  scale_fill_sage_d( name="Missense Mutation\nDeleteriousness Scoring Tools", labels = c('REVEL Score',"VEST Score" )) +
  theme_sage()   + theme_classic() +   facet_wrap(~variable, labeller = labeller(variable = c("revel_oc" = "REVEL Score", "vest_oc" = "VEST Score")))
dev.off()


jpeg("../figures/oc_REVEL_VEST_swnt_patient_1_blood_chr21_overlap.jpeg")
ggplot(df, aes(x=value, fill=variable)) +
  geom_histogram(alpha=0.5, position="identity", bins=25)  +
  labs(x="Score", y="Count", title="Histogram of Scores by Tool (Patient 1 Chr21)")    + theme_classic()   +
  scale_fill_sage_d( name="Missense Mutation\nDeleteriousness Scoring Tools", labels = c('REVEL Score',"VEST Score" )) +
  theme_sage()  
dev.off()
```

```{r}
 # Define thresholds
thresholds <- c(0.1,   0.1, 0.25, 0.33, 0.5, 0.66, 0.75)

# Group data by variable and count values below each threshold
df_counts <- df %>%
  group_by(variable) %>%
  summarise( counts_less_0.1 = sum(value < thresholds[1]),
             counts_0.1 = sum(value >= thresholds[2]),
    counts_0.25 = sum(value  >= thresholds[3]),
            counts_0.33 = sum(value >= thresholds[4]),
            counts_0.5 = sum(value  >= thresholds[5]),
            counts_0.66 = sum(value  >= thresholds[6]),
    counts_0.75 = sum(value >= thresholds[7]),
   )

# View resulting counts
# Melt the data frame to long format
df_melted <- melt(df_counts, id.vars = "variable", variable.name = "threshold", value.name = "count")
# colors <- c("grey","#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "yellow", "purple"  )

jpeg("../figures/oc_REVEL_VEST_swnt_patient_1_blood_chr21_count_at_diff_threshold.jpeg")

# Create the bar plot
ggplot(df_melted, aes(x = variable, y = count, fill = threshold)) +
  scale_color_sage_d() +
  geom_bar(stat = "identity", position = "dodge")  +
  scale_fill_sage_d( labels = c('<0.1',">=0.1" ,">=0.25" ,">=0.33" ,">=0.5" ,">=0.66",  ">=0.75"  )) +
  theme_sage() +
  labs(x = "Variable", y = "Count", title = "Counts of Scores Above Custom Thresholds by Tool") +
  theme_minimal()
dev.off()
```


```{r}
df_revel_vest <- read_tsv('../dataset/test.tsv', col_names  = FALSE)

colnames(df_revel_vest) <- c("chr",  "pos",  "revel_oc", "vest_oc")
df_revel_vest <- df_revel_vest %>% mutate_all(~replace(., . == ".", NA)) %>% na.omit()
df_revel_vest <- df_revel_vest%>% mutate_at(c('revel_oc', 'vest_oc'), as.numeric)
df <- df_revel_vest  %>% melt(id = c('pos', 'chr'))

jpeg("../figures/oc_REVEL_VEST_swnt_patient_1_blood_count.jpeg")
ggplot(df, aes(x=value, fill=variable)) +
  geom_histogram(alpha=0.5, position="identity", bins=25)  +
  labs(x="Score", y="Count", title="Histogram of Scores by Tool (Patient 1)")    +
  scale_fill_sage_d( labels = c('<0.1',">=0.1" ,">=0.25" ,">=0.33" ,">=0.5" ,">=0.66",  ">=0.75"  )) +
  theme_sage() + theme_classic() +   facet_wrap(~variable, labeller = labeller(variable = c("revel_oc" = "REVEL Score", "vest_oc" = "VEST Score")))
dev.off()
```

```{r}
 # Define thresholds
thresholds <- c(0.1, 0.1, 0.25, 0.33, 0.5, 0.66, 0.75)
# Group data by variable and count values below each threshold
df_counts <- df %>% replace(is.na(.), 0)  %>% 
  group_by(variable) %>%
  summarise( counts_less_0.1 = sum(value < thresholds[1]),
             counts_0.1 = sum(value >= thresholds[2]),
    counts_0.25 = sum(value  >= thresholds[3]),
            counts_0.33 = sum(value >= thresholds[4]),
            counts_0.5 = sum(value  >= thresholds[5]),
            counts_0.66 = sum(value  >= thresholds[6]),
    counts_0.75 = sum(value >= thresholds[7]),
   )
```

```{r}
# View resulting counts
# Melt the data frame to long format
df_melted <- melt(df_counts, id.vars = "variable", variable.name = "threshold", value.name = "count")

jpeg("../figures/oc_REVEL_VEST_swnt_patient_1_blood_count_at_diff_threshold.jpeg")

# Create the bar plot
ggplot(df_melted, aes(x = variable, y = count, fill = threshold)) +
  geom_bar(stat = "identity", position = "dodge")    +
  scale_fill_sage_d( labels = c('<0.1',">=0.1" ,">=0.25" ,">=0.33" ,">=0.5" ,">=0.66",  ">=0.75"  )) +
  theme_sage() +
  labs(x = "Variable", y = "Count", title = "Counts of Scores Above Custom Thresholds by Tool") +
  theme_minimal()
dev.off()

```

```{r}
jpeg("../figures/heatscatter_revel_vest.jpeg")
heatscatter(df_revel_vest$revel_oc, df_revel_vest$vest_oc, cor = TRUE, grid =200, xlab = "REVEL score", ylab = "VEST score")+
  theme_sage()
dev.off()
```

```{r}
jpeg("../figures/heatscatter_revel_vest_less_0.25.jpeg")
df_less_0.25 <- df_revel_vest %>% filter(revel_oc <= 0.25, vest_oc <= 0.25)
heatscatter(df_less_0.25$revel_oc, df_less_0.25$vest_oc, cor = TRUE, grid =200, xlab = "REVEL score", ylab = "VEST score")
dev.off()

```
 
 