#!/usr/bin/env Rscript
library(DESeq2)
library(RColorBrewer)
library(cluster)
library(ggplot2)
library(factoextra)
library(synapser)


# get DESeq2 data from Synapse
synLogin()
gtex_data <- synGet("syn50631406")$path
load(gtex_data)
sampleDists <- dist(t(assay(dds)))
sampleDistMatrix <- as.matrix(sampleDists)


# figure out number of clusters to use in k-means clustering
gap_stat <- clusGap(sampleDistMatrix, 
                    FUN = kmeans, 
                    nstart = 25, 
                    K.max = 20, 
                    B = 50,
                    iter.max=50)

# plot cluster statistics
pdf(file = "plots/gtex.cluster_stats.pdf", height = 7, width = 7)
fviz_gap_stat(gap_stat)
fviz_nbclust(sampleDistMatrix, kmeans, method = "wss", k.max = 20)
dev.off()
# 18 clusters looks like okay cutoff point (413/18 ~= 22)

# make k-means clusters
k = 18
set.seed(1)
km <- kmeans(sampleDistMatrix, centers = k, nstart = 25, iter.max = 50)

# look at sizes of clusters
km$size
# [1] 12 29 32 11 22 33  3 22 43  4 27  5 20 35 18 25 26 45

# Cluster plot
pdf(file = "plots/gtex.cluster_visualization.pdf", height = 7, width = 7)
fviz_cluster(km, data = sampleDistMatrix)
dev.off()


# function to print sample names for a selected cluster
cluster_members <- function(cluster_object, cluster_number) {
  file_name <-  paste("cluster_",cluster_number,".txt", sep="")
  sink(file_name)
  cat(names(km$cluster)[km$cluster==cluster_number], sep="\n")
  sink()
}


# print cluster members to text file
for(x in 1:18){
  cluster_members(rm,x)
}



