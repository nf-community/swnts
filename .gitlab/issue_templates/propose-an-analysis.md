## Summary of proposed analysis

### Hypothesis


### What are the scientific goals of the analysis?


### What methods do you plan to use to accomplish the scientific goals?


### What input data are required for this analysis?


### How long to you expect the analysis will take to complete?


### Who will complete the analysis (add GitLab handle if relevant)? Are you looking for additional individuals to work on this analysis?


### What relevant scientific literature relates to this analysis?


### Do you need a place to store your intermediate processing/analysis files?

